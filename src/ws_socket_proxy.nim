import std/[
  asyncnet,
  asyncdispatch,
  logging,
  asynchttpserver,
  strformat
]

import ws, simple_parseopt

addHandler(newConsoleLogger())

var shouldquit = false

type MksClient = ref object of RootObj
  socket: AsyncSocket

let options = get_options:
  target_host: string {.bare, need.}
  target_port: int {.bare, need.}
  websocket_port = 8008

proc connect(mks: MksClient) {.async.} =
  if not mks.socket.isNil() and not mks.socket.isClosed:
    debug "Disconnecting"
    mks.socket.close()
    mks.socket = nil
  mks.socket = await asyncnet.dial(
    options.target_host,
    Port(options.target_port)
  )
  debug fmt"Connected to {options.target_host}:{options.target_port}"

proc main() {.async.} =
  var listeners = newSeq[WebSocket]()
  let mks = MksClient()

  await mks.connect()

  proc on_ws_request(req: Request) {.async, gcsafe.} =
    var ws: WebSocket
    try:
      ws = await newWebSocket(req)
      listeners.add ws
      info fmt"Client {req.hostname} connected"
      while ws.readyState == Open:
        let packet = await ws.receiveStrPacket()
        debug "Forwarding packet: " & packet
        if not mks.socket.isClosed:
          asyncCheck mks.socket.send(packet);
    except WebSocketClosedError:
      info fmt"Client {req.hostname} disconnected"
      let ws_index = listeners.find(ws)
      if ws_index != -1:
        listeners.delete(ws_index)
    except WebSocketProtocolMismatchError:
      error "Socket tried to use an unknown protocol: ", getCurrentExceptionMsg()
    except WebSocketError:
      error "Unexpected socket error: ", getCurrentExceptionMsg()
    await req.respond(Http200, "OK")

  var server = newAsyncHttpServer()
  asyncCheck server.serve(Port(options.websocket_port), on_ws_request)

  while not shouldquit:
    let response = await mks.socket.recvLine()
    if response.len == 0:
      debug "MKS disconnected"
      await mks.connect()
      break

    for listener in listeners:
      if listener.readyState == Open:
        asyncCheck listener.send(response)

proc onCtrlC() {.noconv.} =
  info "Quitting"
  shouldquit = true

setControlCHook(onCtrlC)

waitFor main()
