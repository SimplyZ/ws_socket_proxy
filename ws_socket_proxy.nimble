# Package

version       = "0.1.0"
author        = "Zrean Tofiq"
description   = "A simple tool to serve a normal socket connection over websockets."
license       = "MIT"
srcDir        = "src"
bin           = @["ws_socket_proxy"]
binDir        = "bin"


# Dependencies

requires "nim >= 1.6.6"
requires "ws >= 0.5.0"
requires "simple_parseopt >= 1.1.1"
