FROM nimlang/nim:1.6.6-alpine as build

COPY . .

RUN nimble -y build --passL:-static -d:release

#####################################

FROM scratch

COPY --from=build bin/ws_socket_proxy .

ENTRYPOINT [ "./ws_socket_proxy" ]
